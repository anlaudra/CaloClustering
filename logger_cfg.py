#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date October 2022
@file logger_cfg.py
@brief Configuration for the framework's logging system.
"""

import sys
import logging
import operator as op
from functools import reduce
from enum import Flag, auto
from typing import Optional

def getLogger(name: Optional[str]=None):
    """Returns the logger for given name.

    @param[in] name:
        Name of the logger to be retrieved (typically: __name__)
        If None, retrieves the root logger of the package (Clustering).

    This is just a wrapper around logging.getLogger(...): it ensures
    all logger inherit from a root logger named as the package.
    """
    if name is None:
        return logging.getLogger("CaloClustering")
    return logging.getLogger("CaloClustering." + name)


###############################################################################
# Helpers.

VERBOSE = 15  # Between INFO (20) and DEBUG (10).
logging.VERBOSE = VERBOSE
logging.addLevelName(VERBOSE, "VERBOSE")
def _verbose(self, message, *args, **kwargs):
    if self.isEnabledFor(VERBOSE):
        # pylint: disable=protected-access
        # Yes, logger takes its '*args' as 'args'.
        self._log(VERBOSE, message, args, **kwargs)

logging.Logger.verb = _verbose


class LoggerTypes(Flag):
    """Enumerate which logger should be enabled."""
    NONE = 0
    MAIN = auto()
    CELLS = auto()
    PLANES = auto()
    ALL = MAIN | CELLS | PLANES


###############################################################################
# Loggers setup.

FORMATTER = logging.Formatter(
    # fmt="%(asctime)s %(module)-10s %(levelname)-8s %(funcName)-15s %(message)s",
    # fmt="%(asctime)s %(module)-10s %(levelname)-8s %(message)s",
    fmt="%(asctime)s %(funcName)-18s %(levelname)-8s %(message)s",
    datefmt="%H:%M:%S")
CONSOLE_HANDLER = logging.StreamHandler(sys.stdout)
CONSOLE_HANDLER.setFormatter(FORMATTER)


def setup_logging(
        debug:   Optional[list[str]]=None,
        verbose: Optional[list[str]]=None,
        quiet:   bool=False,
        logfile: Optional[str]=None) -> None:
    """
    @param[in] debug, verbose: list of logger to activate DEBUG/VERBOSE for.
        The string values in the list must be convertible to their
        LoggerTypes values.
    @param[in] quiet: suppress all messages below WARNING.
    @param[logfile]: name of the logfile. No logging to file if None.
    """
    # Setup handlers.
    root_logger = getLogger()
    root_logger.addHandler(CONSOLE_HANDLER)

    if logfile is not None:
        root_logger.warning("Using logfile: %s", logfile)
        file_handler = logging.FileHandler(logfile)
        file_handler.setFormatter(FORMATTER)
        root_logger.addHandler(file_handler)

    # If quiet, don't set anything else.
    if quiet:
        root_logger.setLevel(logging.WARNING)
        return

    # Default level for all loggers.
    root_logger.setLevel(logging.INFO)

    # None input should be considered as default list:
    if debug   is None: debug   = [LoggerTypes.NONE.name]
    if verbose is None: verbose = [LoggerTypes.NONE.name]

    # Take the bitwise OR of the inputs.
    debug   = reduce(op.or_, map(lambda s: LoggerTypes[s], debug))
    verbose = reduce(op.or_, map(lambda s: LoggerTypes[s], verbose))

    loggers = {
        LoggerTypes.MAIN,
        LoggerTypes.CELLS,
        LoggerTypes.PLANES,
    }

    # Apply settings for loggers mentioned in the input.
    for logger_type in loggers:
        if logger_type & verbose:
            getLogger(logger_type.name).setLevel(logging.VERBOSE)
        if logger_type & debug:
            getLogger(logger_type.name).setLevel(logging.DEBUG)

