#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date October 2022

- Read a ROOT file produced by CALICE standalone G4 simulation
(https://stash.desy.de/projects/CALICE/repos/calice_standaloneg4/browse).
- A 3D np.ndarray represents the cell content.
- Each plane is then clusterize...
- ... and plan clusters grouped into 3D clusters.
- The direction of the 3D cluster is finally fitted.

There is a lot of logging in the code, split in 3 separate loggers (inheriting
from a same one). One is the "main", another one for the 2D clustering, and a
third one for the 3D clustering.
The VERBOSE level gives the main steps, while the DEBUG level gives details for
each internal loop iteration and each execution branching.
"""

import sys
from pathlib import Path
from collections import deque
from argparse import ArgumentParser, RawDescriptionHelpFormatter, Action
import logging
from typing import Optional
from collections.abc import Iterator

from math import sqrt
import numpy as np

import ROOT
from CaloModel import CaloGeometry, Cell, PlanarCluster, Cluster3D
from logger_cfg import LoggerTypes, getLogger, setup_logging

import matplotlib
matplotlib.use("Agg")  # Speed up graphics (no display)
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.cm as cmap

# 3 mm tile
MEV2MIP = 0.47 # MIP / MeV
MIP2PE = 15. # p.e / MIP

# 10 mm tile
MEV2MIP *= 10/3 # MIP / MeV
MIP2PE *= 10/3 # p.e / MIP

logger     = getLogger(LoggerTypes.MAIN.name)
log_cells  = getLogger(LoggerTypes.CELLS.name)
log_planes = getLogger(LoggerTypes.PLANES.name)


###############################################################################
# MIND ENERGY IS IN MIP!!!

def get_plot_dim(nplots: int, aspect_ratio: float=1.5) -> tuple[int, int]:
    """Get number of plots in X and Y given a total number of plots and
        an aspect ratio.
    """
    small = round(sqrt(nplots / aspect_ratio))
    large = round(sqrt(nplots * aspect_ratio))
    total = small * large
    if total < nplots:
        if abs((large+1)/small - aspect_ratio) > \
                abs(large/(small+1) - aspect_ratio):
            small += 1
        else:
            large += 1
    return int(large), int(small)


class EventProcessor:
    """Processor"""
    def __init__(self,
            geometry: CaloGeometry=CaloGeometry(),
            seed_threshold: float=2, secondary_threshold: float=0.5,
            do_smear: bool=True, rebin: int=1, do_plot: bool=False) -> None:
        """
        :geometry: geometry description of the calo and cells.
        :seed_threshold: threshold energy in MIP for the cluster seeding.
        :secondary_threshold: minimum cell energy in MIP to be added to
            a cluster.
        :do_smear: flag to apply Poisson smearing of the energy.
        :rebin: rebin calo 
        :do_plot: flag to produce the layer energy plots.
        """
        self._geom = geometry
        logger.warning("CaloGeometry: %s", self._geom)

        self.seed_threshold = seed_threshold
        self.secondary_threshold = secondary_threshold
        self.do_smear = do_smear

        # Hit positions have values in 1..MAX while indices in np array must
        # be in 0..MAX-1.
        self._transformI = lambda i: i-1
        self._transformJ = lambda j: j-1
        self.rebin = rebin

        self.do_plot = do_plot
        # Will be np.array(nCellsX, nCellsY, nLayers)
        # self._original_calo = None
        # self._smeared_calo = None
        # self._calo = None  # Reference to one of the two above.


    @property
    def rebin(self) -> int:
        return self._rebin

    @rebin.setter
    def rebin(self, value) -> None:
        """Defines the transformations hitI/J -> index in np.array.
        
        Also redefines the CaloGeometry accordingly.
        """
        if value < 1 or not isinstance(value, int):
            raise ValueError("Invalid rebin value (must be an int >=1): "
                            f"{type(value)}({value})")
        # If no rebin, do nothing.
        if value == 1:
            return
        self._rebin = value
        self._transformI = lambda i: (i-1) // value
        self._transformJ = lambda j: (j-1) // value
        self._geom = self._geom.rebin(value)
        logger.warning("New geometry: %s", self._geom)

###############################################################################

    def fill_calo(self, event: ROOT.TTree) -> None:
        """Read the cell energies from the ROOT file and fill a 3D array."""
        # pylint: disable=attribute-defined-outside-init

        # Calo representation filled with raw event hits.
        # Build a 3D array out of the TTree information for this event.
        # Combine the various G4 hits within a single cell.
        self._original_calo = np.zeros(
            (self._geom.nCellsX, self._geom.nCellsY, self._geom.nLayers))
        logger.verb("N hits: %d", event.nHits)
        for i_hit in range(event.nHits):
            hitI = self._transformI(event.hitI[i_hit])
            hitJ = self._transformJ(event.hitJ[i_hit])
            hitK = event.hitK[i_hit]-1
            cellID = hitI, hitJ, hitK
            self._original_calo[cellID] += event.hitEnergyBirksG4[i_hit]
        # After poisson smearing. If no smearing, reference the original calo.
        self._smeared_calo = self._original_calo
        # Reference to current calo reprensentation.
        self._calo = self._original_calo

    def _smear_calo(self) -> None:
        """Energy is in MIP, convert to pe, sample with poisson,
            and convert back to MIP.
        """
        # pylint: disable=attribute-defined-outside-init
        logger.verb("Smearing calo with Poisson distribution...")
        self._smeared_calo = np.random.poisson(self._original_calo * MIP2PE) / MIP2PE
        self._calo = self._smeared_calo

    def _make_calo_plot(self, fname: Path) -> None:
        """Save a plot showing the energy map in each layer.

        :fname: output file name.
        """
        # https://stackoverflow.com/questions/54895396/matplotlib-heatmap-with-x-y-data
        # https://stackoverflow.com/questions/13784201/how-to-have-one-colorbar-for-all-subplots
        ncols, nrows = get_plot_dim(self._geom.nLayers)
        fig, axes = plt.subplots(
            nrows=nrows, ncols=ncols, figsize=(25,15), sharex=True, sharey=True)
        # Reshape indexing into 1 dimension for easier looping
        # (figure shape remains the same).
        axes = axes.reshape(-1)
        max_calo = np.max(self._calo)
        for k in range(self._geom.nLayers):
            img = axes[k].imshow(
                self._calo[:,:,k],
                vmin=0,
                vmax=max_calo,
                cmap=cmap.gist_heat_r,
                extent=[0, self._geom.nCellsX, 0, self._geom.nCellsY],
                aspect='equal',
                interpolation='none',
                origin='lower',
            )
            axes[k].set_title(f"Layer {k}")
            axes[k].xaxis.set_major_locator(MultipleLocator(6))
            axes[k].yaxis.set_major_locator(MultipleLocator(6))
        fig.subplots_adjust(top=0.9, bottom=0.1, left=0.1, right=0.8)
        cax = plt.axes([0.85, 0.1, 0.075, 0.8])
        fig.colorbar(mappable=img, cax=cax)  # type: ignore
        # cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        # fig.colorbar(axes[0], ax=axes)
        fig.savefig(fname.with_suffix(".png"))
        plt.close()

    def _surrounding(self, i: int, j: int) -> Iterator[tuple[int, int]]:
        """Returns the (x,y) indices of the 8 cells around i,j."""
        return ( (x,y)
            for x in (i-1, i, i+1)
            for y in (j-1, j, j+1)
            if 0 <= x < self._geom.nCellsX and 0 <= y < self._geom.nCellsY \
                and not (x == i and y == j)
        )

    def _clusterize_cells(self, layer: int) -> list[PlanarCluster]:
        """plane is a np.array[float] of dimension 2."""
        log_cells.verb("Start _clusterize_cells for layer %d...", layer)
        plane = self._calo[:,:,layer]
        # Seed with all cells > seeding threshold
        seeds = list(zip(*np.where(plane > self.seed_threshold)))  # in MIP
        # np.where on a 2d array returns a pair of np.arrays
        # -> zip(*...) to get a list of pairs (x,y)
        log_cells.debug("List of seeds: %s", seeds)

        clusters = []
        for seed_i, seed_j in seeds:
            if any((seed_i, seed_j) in cluster for cluster in clusters):
                log_cells.debug("Seed (%2d,%2d,%2d) with E = %.2f "
                    "is already included in a cluster.",
                    seed_i, seed_j, layer, plane[seed_i, seed_j])
                continue
            log_cells.debug(
                "Starting cluster with seed: (%2d,%2d,%2d), energy = %.2f",
                seed_i, seed_j, layer, plane[seed_i, seed_j])
            cluster = PlanarCluster(layer, [
                Cell(seed_i, seed_j, layer, plane[seed_i,seed_j])
            ])

            candidate_cells = deque(self._surrounding(seed_i, seed_j))
            while candidate_cells:
                i, j = candidate_cells.pop()
                # Avoid double counting.
                if (i,j) in cluster:
                    log_cells.debug("Cell (%2d,%2d,%2d) with energy %.2f "
                        "is already in the current cluster.",
                        i, j, layer, plane[i,j])
                    continue
                # Extend the cluster with all connected cells > secondary threshold.
                if plane[i,j] < self.secondary_threshold:  # in MIP
                    log_cells.debug("Cell (%2d,%2d,%2d) with energy %.2f "
                        "is below threshold (%.2f).",
                        i, j, layer, plane[i,j], self.secondary_threshold)
                    continue
                log_cells.debug(
                    "Cell (%2d,%2d,%2d) with E = %.2f: adding to cluster.",
                    i, j, layer, plane[i,j])
                candidate_cells.extendleft(self._surrounding(i,j))
                cluster.cells.append(Cell(i, j, layer, plane[i,j]))

            log_cells.verb("Built cluster %s", cluster.describe)
            clusters.append(cluster)
        log_cells.verb("End _clusterize_cells...")
        return clusters

    def clusterize_planes(self,
            clusters: dict[int, list[PlanarCluster]]) -> Cluster3D:
        """clusters: dict(layer index -> list of clusters in this layer)"""
        log_planes.verb("Start clusterize_planes...")
        # Seek highest energy remaining cluster.
        initial_cluster = PlanarCluster(-1, [])
        log_planes.debug("Looking for initial cluster...")
        for cluster_list in clusters.values():
            for cluster in cluster_list:
                if cluster.used:
                    log_planes.debug("%s is already used, skipping.", cluster)
                    continue
                if cluster.energy < initial_cluster.energy:
                    log_planes.debug("%s has a lower energy: %.2f",
                                     cluster, initial_cluster.energy)
                    continue
                log_planes.debug("%s has higher energy, selecting!", cluster)
                initial_cluster = cluster
        log_planes.verb("Initial cluster: %s", initial_cluster)
        cluster3D = Cluster3D([initial_cluster])
        initial_cluster.used = True

        # Extend cluster towards the back.
        seed_cluster = initial_cluster
        target_layer = seed_cluster.layer + 1
        while target_layer < self._geom.nLayers:
            log_planes.verb("Target layer %2d with seed cluster: %s",
                            target_layer, seed_cluster)
            next_cluster = self._select_cluster(
                seed_cluster, clusters[target_layer])
            target_layer += 1
            if next_cluster is None:
                continue
            cluster3D.clusters.append(next_cluster)
            seed_cluster = next_cluster

        # Extend cluster towards the front.
        seed_cluster = initial_cluster
        target_layer = seed_cluster.layer - 1
        while target_layer >= 0:
            log_planes.verb("Target layer %2d with seed cluster: %s",
                            target_layer, seed_cluster)
            next_cluster = self._select_cluster(
                seed_cluster, clusters[target_layer])
            target_layer -= 1
            if next_cluster is None:
                continue
            cluster3D.clusters.append(next_cluster)
            seed_cluster = next_cluster

        cluster3D.sort()
        log_planes.verb("Built cluster: %s", cluster3D.describe)
        log_planes.verb("End clusterize_planes...")
        return cluster3D

    def _select_cluster(self,
            seed_cluster: PlanarCluster,
            cluster_pool: list[PlanarCluster]) -> Optional[PlanarCluster]:
        """Determine if and what PlanarCluster should be added."""
        if log_planes.isEnabledFor(logging.DEBUG):
            log_planes.debug("Cluster pool:")
            for cluster in cluster_pool:
                log_planes.debug("%s", cluster)

        sorted_pool = sorted(
            cluster_pool,
            # key=lambda cluster: abs(seed_cluster.bary - cluster.bary))
            key=lambda cluster: cluster.energy, reverse=True)
        log_planes.debug("Sorted input cluster pool.")

        for idx, cluster in enumerate(sorted_pool):
            if cluster.used:
                log_planes.debug("Cluster %2d (%s) has already been used, "
                    "checking next cluster.", idx, cluster)
                continue

            distance = abs(seed_cluster.bary - cluster.bary)
            if distance > 3.5:
                log_planes.debug("Candidate cluster %s is not close enough "
                    "(distance = %.2f > 3.5).", cluster, distance)
                continue

            # Otherwise, we found a good cluster to return.
            log_planes.verb(
                "Adding cluster %d at distance = %.2f < 3.5: %s.",
                distance, idx, cluster)
            cluster.used = True
            return cluster

        log_planes.verb("No cluster left in this layer.")
        return None


    def process(self, event: ROOT.TTree) -> tuple:
        """Main callable."""
        self.fill_calo(event)
        if self.do_smear:
            self._smear_calo()
        if self.do_plot:
            self._make_calo_plot(Path(f"heatmap_ev{event.event}"))

        # Build the list of clusters for each plane.
        logger.verb("Reconstructing clusters in planes...")
        clusters = {
            layer: self._clusterize_cells(layer)
            for layer in range(self._geom.nLayers)
        }

        # Print the clusters.
        logger.debug("Reconstructed clusters:")
        for _, cluster_list in clusters.items():
            for cluster in cluster_list:
                logger.debug(cluster.describe)

        logger.verb("Reconstructing 3D clusters...")
        clusters3D = []
        while not all(
                cluster.used for cluster_list in clusters.values()
                for cluster in cluster_list):
            cluster3D = self.clusterize_planes(clusters)
            cluster3D.fit_direction()
            logger.debug(cluster3D.info_fit)
            clusters3D.append(cluster3D)

        # Sort clusters by decreasing energy.
        clusters3D = sorted(clusters3D, key=lambda x: x.energy, reverse=True)

        # Print list of all clusters to see if we used everything.
        if logger.isEnabledFor(logging.VERBOSE):
            logger.verb("All PlanarCluster available:")
            for _, cluster_list in clusters.items():
                for cluster in cluster_list:
                    logger.verb(cluster)

            logger.verb("All Cluster3D reconstructed:")
            for cluster3D in clusters3D:
                logger.verb(cluster3D.describe)

        logger.info(
            "Event %5d (smeared E = %7.2f): "
            "reco %3d CellCluster (total E = %7.2f) / "
            "%d Clusters3D (energies: %s)",
            event.event,
            self._smeared_calo.sum(),
            sum(len(cluster_list) for cluster_list in clusters.values()),
            sum(cluster.energy
                for cluster_list in clusters.values()
                for cluster in cluster_list),
            len(clusters3D),
            [round(cluster.energy, 2) for cluster in clusters3D]
        )

        px = clusters3D[0].p_x(self._geom) if len(clusters3D) else 0
        py = clusters3D[0].p_y(self._geom) if len(clusters3D) else 0
        pz = clusters3D[0].p_z(self._geom) if len(clusters3D) else 0
        energy = clusters3D[0].energy if len(clusters3D) else 0
        return (
            event.primaryXMomentum, event.primaryYMomentum,
            event.primaryZMomentum, event.primaryParticleEnergy,
            self._original_calo.sum(), self._smeared_calo.sum(),
            px, py, pz, energy,
            sum(len(cluster_list) for cluster_list in clusters.values()),
            sum(cluster.energy
                for cluster_list in clusters.values()
                for cluster in cluster_list),
            len(clusters3D),
        )


def main(file_path: Path, tree: str="Tree",
        do_smear: bool=True, rebin: int=1, do_plot: bool=False,
        first: int=0, nevents: int=0,
        debug: Optional[list[str]]=None, verbose: Optional[list[str]]=None,
        quiet=False, logfile: Optional[str]=None) -> None:
    """Main callable
    
    :file_path: input ROOT file path.
    :tree: tree name to be fetched in the input file.
    :do_smear: see `EventProcessor`
    :rebin: see `EventProcessor`
    :do_plot: see `EventProcessor`
    :first: first event to process.
        Any value <= 0 is normalised to 0.
    :nevents: restrict processing to that many events.
        Any value <= 0 is understood as all events.
    :debug, verbose: list of logger names to activate verbose/debug.
        If None, understood as main logger.
    :quiet: disable all messages below WARNING.
    :logfile: path of the logfile
        - If None, logfile is disbable.
        - If empty string, path is automatically determined from the
          input file.
        - If non-empty: use this path.
        Must use str and not Path because empty path is translated to '.'.
    """
    # outfile_path is named after the input, but in '.' directory.
    outfile_path = Path(file_path.with_suffix("").name + "_results")
    if logfile is not None and not logfile:
        # If logfile must be used but no name provided, use the output file
        # name as logfile name.
        logfile = outfile_path.with_suffix(".log")
    setup_logging(debug=debug, verbose=verbose, quiet=quiet, logfile=logfile)

    # First tests.
    # geometry = CaloGeometry(
    #     nCellsX=84, nCellsY=84, nLayers=30,
    #     layer_thickness=22.55)
    # geometry = CaloGeometry(
    #     nCellsX=42, nCellsY=42, nLayers=40,
    #     pitchX=60, pitchY=60, layer_thickness=20.55)
    # geometry = CaloGeometry(
        # nCellsX=84, nCellsY=84, nLayers=40,
        # pitchX=30, pitchY=30, layer_thickness=20.55)
    geometry = CaloGeometry(
        nCellsX=252, nCellsY=252, nLayers=40,
        pitchX=10, pitchY=10, layer_thickness=35.55)
    processor = EventProcessor(
        geometry=geometry,
        do_smear=do_smear,
        rebin=rebin,
        do_plot=do_plot)

    in_file = ROOT.TFile(str(file_path))
    in_tree = in_file.Get(tree)
    first = max(first, 0)
    if nevents <= 0:
        nevents = in_tree.GetEntries()
    result = []
    iteration = 0
    for event in in_tree:
        iteration += 1
        if iteration % 100 == 1:
            logger.warning("Now at iteration %d", iteration)
        if iteration < first + 1: continue
        if iteration > first + nevents: break
        result.append(processor.process(event))

    columns = {
        "true_px"           : np.float32,
        "true_py"           : np.float32,
        "true_pz"           : np.float32,
        "true_e"            : np.float32,
        "original_e"        : np.float32,
        "smeared_e"         : np.float32,
        "reco_px"           : np.float32,
        "reco_py"           : np.float32,
        "reco_pz"           : np.float32,
        "reco_e"            : np.float32,
        "nPlaneClusters"    : np.int32,
        "clusteredEnergy"   : np.float32,
        "nCluster3D"        : np.int32,
    }
    logger.warning("Building output with columns: %s", columns)
    data = np.asarray(result, dtype=list(columns.items()))
    outfile_path = outfile_path.with_suffix(".npy")
    logger.warning("Writing output file (array size: %s): %s",
                   data.shape, outfile_path)
    np.save(outfile_path, data, allow_pickle=False)
    logger.warning("All done.")


###############################################################################

DESCRIPTION = "Cluster events produced by the AHCAL standalone simulation."
EPILOG = """
See https://stash.desy.de/projects/CALICE/repos/calice_standaloneg4/browse

Expected inputs to this program are the SimpleCalo.root files produced by the
above CALICE simulation (modified to our needs, see future gitlab repo).

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Geometry (number of cells in X and Y, number of layers, cell pitch in X and Y,
and layer thickness) is currently hardcoded in this file.
Such information must be retrieved from the macro file used to run Geant4.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Example:
--------

python Clustering.py SimuInput/221026/SimpleCalo_10m_10GeV.root --rebin 6 --logfile

will output ./SimpleCalo_10m_10GeV_result.npy (and associated logfile),
containing an np.array with named columns with all useful quantities necessary
for energy/momentum resolution analysis.

"""

class NoneOrEmptyStringAction(Action):
    """See https://docs.python.org/3/library/argparse.html#action

    Implements the following behaviour for an option with possibly 0 arguments:
    - If the option is not provided, this class is not called and the
      value to the parameter is the one from `add_argument(default=...)`
      (typically None).
    - If the option is provided but without argument, the option is
      assigned the empty string value.
    - If the option is provided with argument(s), the standard behaviour
      is applied.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        if values is None:
            values = ""
        setattr(namespace, self.dest, values)

def parse_args(argv):
    # pylint: disable=missing-function-docstring
    parser = ArgumentParser(description=DESCRIPTION, epilog=EPILOG,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("file_path", metavar="FILE", type=Path)
    parser.add_argument("-t", "--tree", default="Tree",
        help="Tree name in the input file.")
    parser.add_argument("--no-smear", action="store_false", dest="do_smear",
        help="Deactivate Poisson sampling of photo-electrons.")
    parser.add_argument("--rebin", type=int, default=1,
        help="Merge calo cells in X and Y by this amount.")
    parser.add_argument("--plot", action="store_true", dest="do_plot",
        help="Save a plot of the energy deposits in the calo for each event.")
    parser.add_argument("--first", type=int, default=0,
        help="Event number to start from.")
    parser.add_argument("--nevents", type=int, default=0,
        help="Number of events to process. "
             "If <= 0, process all events.")

    opt_log = parser.add_argument_group("Logging options")
    opt_log.add_argument("--verbose", "-v", action="append", default=["NONE"],
        choices=LoggerTypes.__members__.keys(),
        help="Loggers to set as verbose.")
    opt_log.add_argument("--debug", "-d", action="append", default=["NONE"],
        choices=LoggerTypes.__members__.keys(),
        help="Loggers to set as debug. Take precedence over verbose.")
    opt_log.add_argument("--quiet", "-q", action="store_true",
        help="Suppress all logging < Warning (take precedence over -v/-d).")
    opt_log.add_argument("--logfile", nargs="?",  # Need str, not Path!
        action=NoneOrEmptyStringAction,
        help="Write logfile with name matching the output file. "
             "If option is not provided, no log file is written. "
             "If option is provided without argument, a logfile matching the "
             "name of the input/output file is created. "
             "If option is provided with an argument, it is used as logfile."
        )

    args = parser.parse_args(argv)
    return vars(args)

if __name__ == "__main__":
    main(**parse_args(sys.argv[1:]))

