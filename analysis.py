#!/usr/bin/env
#-*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date October 2022

Produces plots of Clustering inputs and fit energy resolution curve.

For each dataset given in INPUTS, produces plots of true and reconstructed
energy, momentum, and number of clusters. It also fits the energy resolution
for each dataset. For example for an input
`Results/221026/SimpleCalo_10m_10GeV_results.npy`
it will create plots in
`Results/221026/SimpleCalo_10m_10GeV_results/*.pdf`

Once all files in INPUTS have been processed, the energy resolution is extracted
and fitted/plotted as function of the energy of each dataset, providing the
sigmaE/E curve and fit. This plot is written in `Results/221026/resolution.pdf`
(for the example below).
"""

import re
from pathlib import Path
from typing import Optional, Callable

import numpy as np
import pandas as pd
import scipy.stats

from iminuit import Minuit
from iminuit.cost import LeastSquares, UnbinnedNLL

import matplotlib
matplotlib.use("Agg")  # Speed up graphics (no display)
import matplotlib.pyplot as plt


###############################################################################

ENERGY_PATTERN = re.compile(r"_\d+GeV_")
def parse_energy(path: Path) -> float:
    """Extract the energy information from the file name."""
    result = ENERGY_PATTERN.findall(path.stem)
    if not result:
        print("ERROR: no energy pattern found.")
        return -1
    if len(result) > 1:
        print("WARNING: more than one energy pattern found.")
    value = result[0].strip("_")[:-3]
    if value.startswith('0'):
        value = "0." + value[1:]
    print("Matched", result, "into value", value)  # Remove _, GeV, and cast to float.
    return float(value)

DISTANCE_PATTERN = re.compile(r"_\d+m_")
def parse_distance(path: Path) -> float:
    """Extract the distance information from the file name."""
    result = DISTANCE_PATTERN.findall(path.stem)
    if not result:
        print("ERROR: no distance pattern found.")
        return -1
    if len(result) > 1:
        print("WARNING: more than one distance pattern found.")
    return float(result[0].strip("_")[:-1])  # Remove _, m, and cast to float.


###############################################################################

def cleanup_data(data: np.array) -> pd.DataFrame:
    """Cleanup the named array from the input file and return a dataframe."""
    print(f"Removing {len(np.where(data['original_e'] == 0)[0])} "
        "entries with original_e == 0.")
    data = np.delete(
        data, np.where(data["original_e"] == 0)[0], axis=0)
    print(f"Removing {len(np.where(data['smeared_e'] == 0)[0])} "
        "additional entries with smeared_e == 0.")
    data = np.delete(
        data, np.where(data["smeared_e"] == 0)[0], axis=0)
    data = pd.DataFrame(data)
    data["true_pt"] = np.sqrt(data["true_px"] ** 2 + data["true_py"] ** 2)
    data["reco_pt"] = np.sqrt(data["reco_px"] ** 2 + data["reco_py"] ** 2)
    return data


def gauss(x, mu, sigma):
    return scipy.stats.norm.pdf(x, loc=mu, scale=sigma)

def resolution(e, a, b, c):
    return np.sqrt(
        (a / np.sqrt(e)) ** 2 +
        (b / e) ** 2 +
        c ** 2
    )


###############################################################################

class Analysis:
    def __init__(self,
            fname: Path,
            fit_xmin : Optional[float]=None,
            fit_xmax : Optional[float]=None,
            plot_xmin: Optional[float]=None,
            plot_xmax: Optional[float]=None) -> None:
        self._fname = Path(fname)
        self.fit_xmin  = fit_xmin
        self.fit_xmax  = fit_xmax
        self.plot_xmin = plot_xmin
        self.plot_xmax = plot_xmax
        self._data = cleanup_data(np.load(self.fname))
        # self._scale = np.mean( self._data["true_e"] / self._data["reco_e"] )
        self._scale = 1. / np.mean( self._data["reco_e"] / self._data["true_e"] )  # reco_e might be 0...

        self._outdir = self.fname.with_suffix("")
        self._outdir.mkdir(exist_ok=True)
        print("Created outdir:", self._outdir)


    @property
    def energy(self) -> float:
        return parse_energy(self.fname)

    @property
    def distance(self) -> float:
        return parse_distance(self.fname)

    # Ensure fname is type Path.
    @property
    def fname(self) -> Path:
        return Path(self._fname)

    @fname.setter
    def fname(self, value) -> None:
        self._fname = Path(value)

    def _add_fname(self, ax: plt.Axes) -> None:
        ax.text(0.05, 1.02, self.fname, transform=ax.transAxes)


    ###########################################################################

    def plot_true_e(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['true_e'], bins=50)
        ax.set_xlabel("True energy [MeV]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['true_e']):.4g} | Stdev = {np.std(self._data['true_e']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "trueE.pdf")
        plt.close()

    def plot_reco_e(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['reco_e'], bins=50)
        ax.set_xlabel("Reco energy [MIP]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['reco_e']):.4g} | Stdev = {np.std(self._data['reco_e']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "recoE.pdf")
        plt.close()

    def plot_ratio_recoEscaled_trueE(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        data = self._data['reco_e'] * self._scale / self._data['true_e']
        ax.hist(data, bins=50)
        ax.set_xlabel(f"E_reco * {self._scale:.4g} / E_true")
        ax.grid()

        info = f"Mean = {np.mean(data):.4g} | Stdev = {np.std(data):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "ratio_recoEscaled_trueE.pdf")
        plt.close()

    def plot_calo_e(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['original_e'],       bins=50, alpha=0.5, label="original")
        ax.hist(self._data['smeared_e'],        bins=50, alpha=0.5, label="smeared")
        ax.hist(self._data['clusteredEnergy'],  bins=50, alpha=0.5, label="clustered")
        ax.set_xlabel("Energy [MIP]")
        ax.grid()
        ax.legend()

        info = "\n".join([
            f"Original : Mean = {np.mean(self._data['original_e'])      :.4g} | Stdev = {np.std(self._data['original_e'])      :.4g}",
            f"Smeared  : Mean = {np.mean(self._data['smeared_e'])       :.4g} | Stdev = {np.std(self._data['smeared_e'])       :.4g}",
            f"Clustered: Mean = {np.mean(self._data['clusteredEnergy']) :.4g} | Stdev = {np.std(self._data['clusteredEnergy']) :.4g}",
        ])
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "caloE.pdf")
        plt.close()

    def plot_ratio_smearedE_originalE(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        data = self._data['smeared_e'] / self._data['original_e']
        ax.hist(data, bins=50)
        ax.set_xlabel("Smeared / original energy")
        ax.grid()

        info = f"Mean = {np.mean(data):.4g} | Stdev = {np.std(data):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "ratio_smearedE_originalE.pdf")
        plt.close()

    def plot_ratio_clusteredE_smearedE(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        data = self._data['clusteredEnergy'] / self._data['smeared_e']
        ax.hist(data, bins=50)
        ax.set_xlabel("Clustered / smeared energy")
        ax.grid()

        info = f"Mean = {np.mean(data):.4g} | Stdev = {np.std(data):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "ratio_clusteredE_smearedE.pdf")
        plt.close()

    def plot_nPlanarClusters(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['nPlaneClusters'], bins=50, range=(0,50))
        ax.set_xlabel("# reco 2D clusters")
        ax.grid()

        info = f"Mean = {np.mean(self._data['nPlaneClusters']):.4g} | Stdev = {np.std(self._data['nPlaneClusters']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "nPlanarCluster.pdf")
        plt.close()

    def plot_nCluster3D(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['nCluster3D'], bins=50, range=(0,10))
        ax.set_xlabel("# reco 2D clusters")
        ax.grid()

        info = f"Mean = {np.mean(self._data['nCluster3D']):.4g} | Stdev = {np.std(self._data['nCluster3D']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "nPlanarCluster.pdf")
        plt.close()

    def plot_true_px(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['true_px'], bins=50)
        ax.set_xlabel("True Px [MeV]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['true_px']):.4g} | Stdev = {np.std(self._data['true_px']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "truePx.pdf")
        plt.close()

    def plot_reco_px(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['reco_px'], bins=50)
        ax.set_xlabel("Reco Px [MIP]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['reco_px']):.4g} | Stdev = {np.std(self._data['reco_px']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "recoPx.pdf")
        plt.close()

    def plot_true_py(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['true_py'], bins=50)
        ax.set_xlabel("True Py [MeV]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['true_py']):.4g} | Stdev = {np.std(self._data['true_py']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "truePy.pdf")
        plt.close()

    def plot_reco_py(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['reco_py'], bins=50)
        ax.set_xlabel("Reco Py [MIP]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['reco_py']):.4g} | Stdev = {np.std(self._data['reco_py']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "recoPy.pdf")
        plt.close()

    def plot_true_pt(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['true_pt'], bins=50)
        ax.set_xlabel("True Pt [MeV]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['true_pt']):.4g} | Stdev = {np.std(self._data['true_pt']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "truePt.pdf")
        plt.close()

    def plot_reco_pt(self) -> None:
        fig, ax = plt.subplots(figsize=(8,6))
        ax.hist(self._data['reco_pt'], bins=50)
        ax.set_xlabel("Reco Pt [MIP]")
        ax.grid()

        info =  f"Mean = {np.mean(self._data['reco_pt']):.4g} | Stdev = {np.std(self._data['reco_pt']):.4g}"
        ax.text(0.05, 0.90, info, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "recoPt.pdf")
        plt.close()

    def make_all_plots(self):
        self.plot_true_e()
        self.plot_reco_e()
        self.plot_ratio_recoEscaled_trueE()
        self.plot_calo_e()
        self.plot_ratio_smearedE_originalE()
        self.plot_ratio_clusteredE_smearedE()
        self.plot_nPlanarClusters()
        self.plot_nCluster3D()
        self.plot_true_px()
        self.plot_reco_px()
        self.plot_true_py()
        self.plot_reco_py()
        self.plot_true_pt()
        self.plot_reco_pt()


    def fit_energy_resolution(self,
            model: Callable[[float, float, float], float]) -> Minuit:
        """Fit (gaussian) and plot the energy resolution for a given dataset."""
        print("Fitting:", self.fname)
        data = self._data['reco_e'] * self._scale / self._data['true_e']
        data_fit = np.copy(data)
        if self.fit_xmin is not None:
            data_fit = data_fit[ data_fit > self.fit_xmin ]
        if self.fit_xmax is not None:
            data_fit = data_fit[ data_fit < self.fit_xmax ]

        if self.fit_xmin  is None: self.fit_xmin  = np.min(data)
        if self.fit_xmax  is None: self.fit_xmax  = np.max(data)
        if self.plot_xmin is None: self.plot_xmin = np.min(data)
        if self.plot_xmax is None: self.plot_xmax = np.max(data)

        nll = UnbinnedNLL(data_fit, model)
        minuit = Minuit(nll, mu=1, sigma=0.1)
        minuit.migrad()
        print(minuit)

        stats = "\n".join([
            f"Mean = {data.mean():.3g}",
            f"RMS  = {data.var():.3g}"
        ])

        params = "\n".join([
            f"$\\mu = {minuit.values[0]:.3g} \\pm {minuit.errors[0]:.3g}$",
            f"$\\sigma = {minuit.values[1]:.3g} \\pm {minuit.errors[1]:.3g}$",
        ])

        fig, ax = plt.subplots(figsize=(8,6))
        # Plot data.
        content, bins = np.histogram(data, bins=50, range=(self.plot_xmin, self.plot_xmax))
        centres = (bins[1:] + bins[:-1]) / 2  # type: ignore
        ax.errorbar(centres, content, yerr=np.sqrt(content), fmt="ok")
        # plt.plot(data, np.zeros_like(data), "|", alpha=0.05)

        # Plot fit: full range, then fit range only.
        xplot = np.linspace(self.plot_xmin, self.plot_xmax, 201)
        ax.plot(xplot, model(xplot, *minuit.values) * len(data_fit) * (centres[1] - centres[0]), ls="--")
        xfit = np.linspace(self.fit_xmin, self.fit_xmax, 201)
        ax.plot(xfit, model(xfit, *minuit.values) * len(data_fit) * (centres[1] - centres[0]))
        ax.grid()
        ax.set_xlabel(f"E_reco * {self._scale:.4g} / E_true")

        ax.text(0.05, 0.80, params, transform=ax.transAxes)
        ax.text(0.80, 0.80, stats, transform=ax.transAxes)
        self._add_fname(ax)
        fig.savefig(self._outdir / "fit_energy_resolution.pdf")
        plt.close()

        return minuit


###############################################################################

# INPUTS = [
#     Analysis("Results/221022/SimpleCalo_10m_1GeV_results.npy", fit_xmin=0.65, fit_xmax=1.35, plot_xmin=0.40, plot_xmax=1.60),
#     Analysis("Results/221022/SimpleCalo_10m_2GeV_results.npy", fit_xmin=0.75, fit_xmax=1.25, plot_xmin=0.60, plot_xmax=1.40),
#     Analysis("Results/221022/SimpleCalo_10m_5GeV_results.npy", fit_xmin=0.87, fit_xmax=1.13, plot_xmin=0.75, plot_xmax=1.25),
#     Analysis("Results/221022/SimpleCalo_10m_10GeV_results.npy", fit_xmin=0.90, fit_xmax=1.10, plot_xmin=0.85, plot_xmax=1.15),
#     Analysis("Results/221022/SimpleCalo_10m_15GeV_results.npy", fit_xmin=0.90, fit_xmax=1.10, plot_xmin=0.85, plot_xmax=1.15),
#     Analysis("Results/221022/SimpleCalo_10m_20GeV_results.npy", fit_xmin=0.92, fit_xmax=1.08, plot_xmin=0.85, plot_xmax=1.15),
#     Analysis("Results/221022/SimpleCalo_10m_25GeV_results.npy", fit_xmin=0.93, fit_xmax=1.07, plot_xmin=0.85, plot_xmax=1.15),
#     Analysis("Results/221022/SimpleCalo_10m_30GeV_results.npy", fit_xmin=0.93, fit_xmax=1.07, plot_xmin=0.85, plot_xmax=1.15),
# ]

INPUTS = [
    Analysis("Results/221026/SimpleCalo_10m_05GeV_results.npy", fit_xmin=0.40, fit_xmax=1.60, plot_xmin=0.20, plot_xmax=1.80),
    Analysis("Results/221026/SimpleCalo_10m_1GeV_results.npy", fit_xmin=0.65, fit_xmax=1.35, plot_xmin=0.40, plot_xmax=1.60),
    Analysis("Results/221026/SimpleCalo_10m_2GeV_results.npy", fit_xmin=0.75, fit_xmax=1.25, plot_xmin=0.60, plot_xmax=1.40),
    Analysis("Results/221026/SimpleCalo_10m_5GeV_results.npy", fit_xmin=0.87, fit_xmax=1.13, plot_xmin=0.75, plot_xmax=1.25),
    Analysis("Results/221026/SimpleCalo_10m_10GeV_results.npy", fit_xmin=0.90, fit_xmax=1.10, plot_xmin=0.85, plot_xmax=1.15),
    Analysis("Results/221026/SimpleCalo_10m_15GeV_results.npy", fit_xmin=0.90, fit_xmax=1.10, plot_xmin=0.85, plot_xmax=1.15),
    Analysis("Results/221026/SimpleCalo_10m_20GeV_results.npy", fit_xmin=0.92, fit_xmax=1.08, plot_xmin=0.85, plot_xmax=1.15),
]

def get_sigma(minuit: Minuit) -> tuple[float, float]:
    """Retrieve sigma and sigma_err from Minuit instance."""
    return minuit.values[1], minuit.errors[1]


def fit_resolution(
        energies: np.ndarray, sigmas: np.ndarray, errs: np.ndarray,
        ofdir: Path) -> None:
    print("fitting resolution...")
    lsq = LeastSquares(energies, sigmas, errs, resolution)
    minuit = Minuit(lsq, a=0.05, b=0.01, c=0)
    minuit.fixed["c"] = True
    minuit.migrad()
    print(minuit)

    params = "\n".join([
        r"$\frac{\sigma_E}{E} = \frac{a}{\sqrt{E}} \oplus \frac{b}{E} \oplus c$",
        "",
        f"$a = {minuit.values[0]:.3g} \\pm {minuit.errors[0]:.3g}$",
        f"$b = {minuit.values[1]:.3g} \\pm {minuit.errors[1]:.3g}$",
        f"$c = {minuit.values[2]:.3g} \\pm {minuit.errors[2]:.3g}$",
    ])

    xs = np.linspace(min(energies), max(energies), 201)
    ys = resolution(xs, *minuit.values)
    fig, ax = plt.subplots(figsize=(8,6))
    ax.errorbar(energies, sigmas, errs, fmt="ok", label="data")
    ax.plot(xs, ys, label="fit")
    ax.text(0.7, 0.8, params, transform=ax.transAxes)
    ax.set_xlabel("E [GeV]")
    ax.set_ylabel("$\\sigma_E / E$")
    ax.set_title("Single photon energy resolution")
    ax.grid()

    fname = ofdir / "resolution.pdf"
    print("Saving file:", fname)
    fig.savefig(fname)


def main():
    result = {}
    for ana in INPUTS:
        print("Starting file:", ana.fname)
        ana.make_all_plots()
        minuit = ana.fit_energy_resolution(gauss)
        result[ana.energy] = get_sigma(minuit)
        print("=" * 120)

    energies = np.array(list(result.keys()))
    sigmas   = np.array([ val[0] for val in result.values() ])
    errs     = np.array([ val[1] for val in result.values() ])

    fit_resolution(energies, sigmas, errs, INPUTS[0].fname.parent)


if __name__ == "__main__":
    main()
