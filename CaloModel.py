#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date October 2022

Classes defining
- Geometry: the calorimeter geomery
- Cell
- PlanarCluster: a 2D cluster contained in a single layer
- Cluster3D: a set of plane clusters representing a shower.
"""

from math import sqrt
from dataclasses import dataclass
from functools import cached_property
from typing import ClassVar

import numpy as np
from iminuit import Minuit

from fit_utils import LeastSquares, info_fit
from logger_cfg import LoggerTypes, getLogger

logger = getLogger(LoggerTypes.MAIN.name)


###############################################################################
# MIND ENERGY IS IN MIP!!!

@dataclass
class CaloGeometry:
    """Container class for calorimeter geometry."""
    nCellsX: int=24
    nCellsY: int=24
    nLayers: int=40
    pitchX: float=30.  # mm
    pitchY: float=30.  # mm
    layer_thickness: float=20.9  # mm

    def __str__(self) -> str:
        return (f"Calo: {self.nCellsX}x{self.nCellsY} cells (X,Y) "
                f"with {self.nLayers} layers. "
                f"Pitch = ({self.pitchX:.2f} x {self.pitchY:.2f}) mm, "
                f"layer thickness = {self.layer_thickness:.2f} mm."
        )

    def rebin(self, factor) -> "CaloGeometry":
        return CaloGeometry(
            nCellsX = int(np.ceil(self.nCellsX / factor)),
            nCellsY = int(np.ceil(self.nCellsY / factor)),
            nLayers = self.nLayers,
            pitchX  = self.pitchX * factor,
            pitchY  = self.pitchY * factor,
            layer_thickness = self.layer_thickness
        )



@dataclass
class Cell:
    """Class representing a Cell: 3 positions + energy."""
    i: int
    j: int
    k: int
    energy: float

    def __str__(self) -> str:
        # pylint: disable=missing-function-docstring
        return (f"Cell {self.i:02d}, {self.j:02d}, {self.k:02d}: "
                f"E = {self.energy:5.2f} [MIP]")

    def __complex__(self) -> complex:
        """Get i,j coordinates as complex to ease computations."""
        return complex(self.i, self.j)


class PlanarCluster:
    """Collection of cells."""
    number: ClassVar[int] = 0

    def __init__(self, layer: int, cells: list[Cell]) -> None:
        self.layer = layer
        self.cells = cells
        self.used = False
        self.id = PlanarCluster.number
        PlanarCluster.number += 1

    @cached_property
    def energy(self) -> float:
        """Sum of cells' energies."""
        return sum(cell.energy for cell in self.cells)

    @cached_property
    def bary(self) -> complex:
        """Barycentre of the cluster."""
        return sum(
                complex(cell) * cell.energy for cell in self.cells
            ) / self.energy

    def __contains__(self, elem: tuple[int, int]) -> bool:
        """Returns whether a cell is already contained in the cluster."""
        return elem in ( (cell.i, cell.j) for cell in self.cells )

    def __str__(self) -> str:
        # pylint: disable=missing-function-docstring
        return (f"PlaneCluster {self.id:3d} (used={self.used:d}): "
                f"layer {self.layer:2d}, nCell = {len(self.cells):2d}, "
                f"E = {self.energy:6.2f} [MIP], "
                f"bary = {self.bary.real:5.2f}, {self.bary.imag:5.2f} [cell]")

    @cached_property
    def describe(self) -> str:
        """Long description with all the cells."""
        return "\n    ".join([str(self)] + [str(cell) for cell in self.cells])


class Cluster3D:
    """Collection of PlanarClusters."""
    def __init__(self, clusters: list[PlanarCluster]) -> None:
        self.clusters = clusters
        # self.minuit = None  # Will be a Minuit instance, or None if no fit.

    def sort(self) -> None:
        """Sort cluster list by layer index."""
        self.clusters = sorted(self.clusters, key=lambda cluster: cluster.layer)

    @cached_property
    def energy(self) -> float:
        """Sum of energies of the PlanarClusters."""
        return sum(cluster.energy for cluster in self.clusters)

    @cached_property
    def cog(self) -> tuple[float, float, float]:
        """Centre of gravity (x,y,z) of the 3D cluster."""
        xy = sum(
            cluster.bary * cluster.energy for cluster in self.clusters
        ) / self.energy
        z = sum(
            cluster.layer * cluster.energy for cluster in self.clusters
        ) / self.energy
        return xy.real, xy.imag, z

    def __str__(self) -> str:
        # pylint: disable=missing-function-docstring
        return (f"Cluster3D: COG = {self.cog[0]:5.2f}, {self.cog[1]:5.2f}, "
                f"{self.cog[2]:5.2f} [cell], E = {self.energy:6.2f} [MIP], "
                f"using {len(self.clusters)} PlanarClusters: "
                f"{[cluster.id for cluster in self.clusters]}")

    @cached_property
    def describe(self) -> str:
        """Long description with all the PlanarClusters."""
        return "\n    ".join(
            [str(self)] + [str(cluster) for cluster in self.clusters])

    def fit_direction(self) -> None:
        """Fit direction of the shower."""
        self.minuit = None
        if len(self.clusters) < 3:
            logger.debug("This Cluster3D doesn't have enough layers.")
            return

        # xs, ys, ws = np.array([
        data = np.array([
            (cluster.layer, cluster.bary, cluster.energy)
            for cluster in self.clusters ]).T
        # dtype=complex128, didn't succeed in casting to lower size.

        def model(z, entry_i, entry_j, mom_i, mom_j):
            """
            :z: plane index
            :entry_i, entry_j: entry point in the calorimeter (first plane),
                in cell index.
            :mom_i, mom_j: momentum in i/j directions.
            """
            return z * complex(mom_i, mom_j) + complex(entry_i, entry_j)

        # Everything was transformed to complex in `data`, but layer and weight
        # are in fact real. Take the real part to avoid warnings.
        lsq = LeastSquares(model, data[0].real, data[1], data[2].real)
        self.minuit = Minuit(lsq,
            entry_i=self.cog[0],
            entry_j=self.cog[1],
            mom_i=(
                (self.clusters[-1].bary.real - self.clusters[0].bary.real) /
                (self.clusters[-1].layer - self.clusters[0].layer)
            ),
            mom_j=(
                (self.clusters[-1].bary.imag - self.clusters[0].bary.imag) /
                (self.clusters[-1].layer - self.clusters[0].layer)
            )
        )
        self.minuit.migrad()

    @property
    def info_fit(self) -> str:
        """Multiline string with all fit informations."""
        if self.minuit is None:
            return "Not fitted " + str(self)
        return info_fit(self.minuit)

    def p_x(self, geom: CaloGeometry) -> float:
        """Momentum in the x direction."""
        if self.minuit is None:
            logger.error("Trying to access non-fitted cluster. Returning 0.")
            return 0
        return self.minuit.values["mom_i"] * geom.pitchX / geom.layer_thickness

    def p_y(self, geom: CaloGeometry) -> float:
        """Momentum in the y direction."""
        if self.minuit is None:
            logger.error("Trying to access non-fitted cluster. Returning 0.")
            return 0
        return self.minuit.values["mom_j"] * geom.pitchY / geom.layer_thickness

    def p_z(self, geom: CaloGeometry) -> float:
        """Momentum in the z direction."""
        if self.minuit is None:
            logger.error("Trying to access non-fitted cluster. Returning 0.")
            return 0
        return sqrt(self.energy**2 - self.p_x(geom)**2 - self.p_y(geom)**2)


