#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date October 2022

Utils for Minuit.
"""

from typing import ClassVar

import numpy as np
from iminuit import Minuit
from iminuit.util import make_func_code, describe
from tabulate import tabulate

from logger_cfg import LoggerTypes, getLogger

logger = getLogger(LoggerTypes.MAIN.name)


###############################################################################
# Minuit fit

class LeastSquares:
    """
    Least-squares cost function for complex plane.
    See https://iminuit.readthedocs.io/en/stable/notebooks/generic_least_squares.html
    """

    # for Minuit to compute errors correctly
    errordef: ClassVar = Minuit.LEAST_SQUARES

    def __init__(self, model, x, y, w):
        """
        x: np.array(float), layer indices of the clusters
        y: np.array(complex), barycentres of the clusters
        w: np.array(float), energy of the clusters
        """
        self.model = model  # model predicts y for given x
        self.x = np.asarray(x)
        self.y = np.asarray(y)
        self.w = np.asarray(w)
        self.func_code = make_func_code(describe(model)[1:])

    @property
    def ndata(self) -> int:
        """Must be a property named 'ndata'."""
        return 2 * self.y.size  # x2 since we have complexes.

    def __call__(self, *args) -> float:
        """Chi2 to minimize.

        Minimise the sum of the squared distances to the barycentres,
        weighted by the energy of the clusters.
        This corresponds to a Chi2 with sigma = 1/sqrt(energy).

        WARNING: The uncertainties on the fitted parameters are unphysical.
        They can be scaled arbitrarily by the sum of energies.
        """
        val = np.sum(
            np.abs(self.model(self.x, *args) - self.y) ** 2 * self.w
        ) / np.sum(self.w)
        logger.debug("%g, " * len(args) + "-> %g", *args, val)
        return val


def info_fit(minuit: Minuit) -> str:
    has_cov = "yes" if minuit.fmin.has_covariance else "NO"
    cov_accurate = "accurate" if minuit.fmin.has_accurate_covar else "NOT ACCURATE"
    cov_posdef = "posdef" if minuit.fmin.has_posdef_covar else "NOT POSDEF"
    cov_forced = "FORCED" if minuit.fmin.has_made_posdef_covar else "not forced"
    edm_sign = "<" if minuit.fmin.edm < minuit.fmin.edm_goal else ">"
    return "\n".join([
        " | ".join([
            f"Valid: {minuit.fmin.is_valid}",
            f"edm: {minuit.fmin.edm:.4g} {edm_sign} {minuit.fmin.edm_goal:.4g}",
            f"cov {has_cov}: {cov_accurate}, {cov_posdef}, {cov_forced}",
        # ]),
        # " | ".join([
            f"nfcn = {minuit.nfcn}, ngrad = {minuit.ngrad}",
            f"fval = {minuit.fval:.4g}, ndf = {minuit.ndof}, "
            f"chi2/ndf = {minuit.fmin.reduced_chi2:.4g}"
            # f"time = {minuit.fmin.time}"  # Not in this version.
        ]),
        tabulate(*minuit.params.to_table()),
        "",
        tabulate(*minuit.covariance.to_table()),
        "",
        str(minuit.values),
        str(minuit.errors),
        "",
    ])

